<?php
require_once("includes/functions.inc.php");

if(isset($_GET['id']))
{
    $id = $_GET['id'];
    // dd($id);
    //Fetch Image name & delete the image
    $row = db_select("SELECT * FROM contacts WHERE id = {$id}");
    if($row == false){
        $error = "Sorry the record which you are trying to find does not exists!";
        header("Location: index.php?q=error&op=del");
        // dd($error);
    }

    $image_name = $row[0]['image_name'];
    // console.log($image_name);
    unlink("image/users/{$image_name}");

    $result = db_query("DELETE FROM contacts WHERE id={$id}");
	if(!$result)
	{
		header("Location: index.php?q=error&op=del");
	}else{
		header("Location: index.php?q=success&op=del");
	}
}