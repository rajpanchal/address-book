# Address Book
Address Book in PHP using POP approach 

## Build With
1. HTML
2. CSS
3. JQuery v3.4.1
4. PHP

## Plugins Used
1. [Materialize CSS](https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css)
2. [jQuery Validation Plugin v1.14.0](http://jqueryvalidation.org/)

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)
